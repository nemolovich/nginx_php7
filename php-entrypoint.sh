#!/bin/sh
#

echo "PHP-FPM is now starting..."
docker-php-entrypoint php-fpm &
php_pid="$!"

if [ ! -f /var/www/html/index.php ] ; then
  echo "Creating default page..."
  cp /root/index.php /var/www/html/index.php
fi

echo "NGINX is now starting..."

nginx -g "daemon off;" &
nginx_pid="$!"
trap "echo 'Stopping container'; kill -SIGTERM ${nginx_pid}; kill -SIGTERM ${php_pid}" SIGINT SIGTERM

while kill -0 ${php_pid} > /dev/null 2>&1 || kill -0 ${nginx_pid} > /dev/null 2>&1 ; do
  wait
done
