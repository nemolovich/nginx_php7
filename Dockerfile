FROM php:7-fpm
LABEL maitainer "bgohier<brian.gohier@capgemini.com>"

RUN apt-get update \
  && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libldap2-dev \
    libmcrypt-dev \
    default-mysql-client \
    zlib1g-dev \
    libzip-dev \
    zip \
    nginx \
  && apt-get -y clean && apt-get -y autoremove \
  && rm -rf /var/lib/apt/lists/* && rm -rf /usr/share/doc

RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
  && docker-php-ext-configure gd --with-freetype --with-jpeg \
  && printf "\n" | pecl install mcrypt \
  && docker-php-ext-enable mcrypt \
  && docker-php-ext-install -j$(nproc) gd ldap mysqli pdo pdo_mysql zip

RUN conf=/etc/ldap/ldap.conf \
  && mkdir $(dirname $conf) \
  && echo "TLS_CACERT      /etc/ssl/certs/ca-certificates.crt" > $conf

EXPOSE 8080

STOPSIGNAL SIGTERM

COPY php.ini /usr/local/etc/php/php.ini

COPY ./nginx.conf /etc/nginx/nginx.conf

COPY ./index.php /root/index.php

COPY ./php-entrypoint.sh /root/php-entrypoint.sh

RUN chmod +x /root/php-entrypoint.sh

ENTRYPOINT '/root/php-entrypoint.sh'
