# PHP7 server on NGINX

Simply deploy PHP server with NGINX.

## With standard docker

### Build

Build the container with:

```
docker build -t php7_server ./
```

### Run

Run the server with:

```
docker run -d --name php7 \
  -v /opt/volumes/ca.pem:/etc/ssl/certs/ca-certificates.crt:ro
  -v /opt/volumes/www:/var/www/html:Z \
  php7_server
```

The volume mount on `/etc/ssl/certs/ca-certificates.crt` is to allow certificates for LDAP for example.

## Access to web site

You can now access to your PHP site on container IP on the mapped port
(default port 8080). The phpinfo() page is displayed by default.

eg: http://localhost:8080
